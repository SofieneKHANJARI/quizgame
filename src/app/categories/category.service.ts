import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from './category';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  baseAPI= "/assets/categories.json";
  
  constructor(private http: HttpClient) { }

  getAllCategories():Observable<Array<Category>>{
    return this.http.get<Array<Category>>(this.baseAPI);
  }

  getCategory(id: number):Observable<Category>{
    return this.http.get<Category>(this.baseAPI);
  }

}
