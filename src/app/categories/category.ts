export interface Category {
  id: number;
  category: string;
  questions: string;
  color: string;
  image: string;
}
