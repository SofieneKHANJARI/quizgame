import { Component } from '@angular/core';
import { Category } from '../category';
import { CategoryService } from '../category.service';
import { CategoryComponent } from '../category/category.component';

@Component({
  selector: 'app-list-categories',
  standalone: true,
  imports: [CategoryComponent],
  templateUrl: './list-categories.component.html',
  styleUrl: './list-categories.component.css'
})
export class ListCategoriesComponent {

  categories!: Category[];

  constructor( private categoryService: CategoryService){}

  getCategories(){
    this.categoryService.getAllCategories().subscribe(
      (data) => {
        this.categories=data;
        console.log(data);
        
      })
      
    }

  ngOnInit(){
    this.getCategories();
  }

}
