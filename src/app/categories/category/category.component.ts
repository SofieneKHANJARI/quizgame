import { Component, Input } from '@angular/core';
import { Category } from '../category';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-category',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './category.component.html',
  styleUrl: './category.component.css'
})
export class CategoryComponent {
  @Input() category!: Category;

  // constructor(private router: Router){}

  // navigate(){
  //   this.router.navigate(['card']);
  // }
}
