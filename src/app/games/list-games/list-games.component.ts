import { Component } from '@angular/core';
import { GameComponent } from '../game/game.component';
import { Game } from '../game';
import { GameServiceService } from '../game-service.service';

@Component({
  selector: 'app-list-games',
  standalone: true,
  imports: [GameComponent],
  templateUrl: './list-games.component.html',
  styleUrl: './list-games.component.css'
})
export class ListGamesComponent {

  games!: Game[];

  constructor( private gameService: GameServiceService){}

  getCategories(){
    this.gameService.getCategories().subscribe(
      (data) => {
        this.games=data;
        console.log(data);
        
      })
      
    }

  ngOnInit(){
    this.getCategories();
  }


}
