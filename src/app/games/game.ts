export interface Game {
    id: number;
    name: string;
    description: string;
    logo: string;
    rate: number;
    color: string;

}
