import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Game } from './game';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameServiceService {

  baseAPI= "/assets/games.json";

  constructor(private http: HttpClient) { }

 
  getCategories():Observable<Array<Game>>{
    return this.http.get<Array<Game>>(this.baseAPI);
  }

  getCategory(id: number):Observable<Game>{
    return this.http.get<Game>(this.baseAPI);
  }




}
