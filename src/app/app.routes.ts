import { Routes } from '@angular/router';
import { ListGamesComponent } from './games/list-games/list-games.component';
import { ListCategoriesComponent } from './categories/list-categories/list-categories.component';
import { ListCardsComponent } from './cards/list-cards/list-cards.component';

export const routes: Routes = [
    {path:"", component: ListGamesComponent},
    {path:"categories", component: ListCategoriesComponent},
    {path:"cards", component: ListCardsComponent}

];
