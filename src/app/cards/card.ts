export interface Card {
    id: number;
    category_id: number;
    question: string;
    answer: string;
    propositions: string[];
    argument: string;
    score: number;
    color: string;
}
