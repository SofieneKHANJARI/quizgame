import { Component, Input } from '@angular/core';
import { Card } from '../card';
import { CommonModule } from '@angular/common';
import { QuestionComponent } from '../question/question.component';


@Component({
  selector: 'app-card',
  standalone: true,
  imports: [CommonModule, QuestionComponent],
  templateUrl: './card.component.html',
  styleUrl: './card.component.css'
})
export class CardComponent {
  @Input() card !: Card;
  

}
