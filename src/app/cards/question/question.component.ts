import { Component } from '@angular/core';
import { Card } from '../card';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-question',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './question.component.html',
  styleUrl: './question.component.css'
})
export class QuestionComponent {

  
  question: Card=   {
    id: 7,
    category_id: 207,
    question: "ما هو اليوم المقدس في الأسبوع عند المسلمين؟",
    answer: "الجمعة",
    propositions: ["السبت", "الخميس", "الجمعة"],
    argument: "الجمعة هو اليوم المقدس في الأسبوع عند المسلمين.",
    score: 10,
    color: "#4B0082"
  }

  constructor(private route: Router ){

  }

  setAnswer(value: string){
    if (this.question.answer== value){
      console.log("yes");
    }
    else{
      console.log("no");
    }
    console.log(value);

    this.route.navigate(['categories']);   //// a rectifier .. ici il faut faire l'actualisation dans le mm compo.
    
  }

} 
