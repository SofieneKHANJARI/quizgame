import { Component } from '@angular/core';
import { Card } from '../card';
import { CardService } from '../card.service';
import { CardComponent } from '../card/card.component';

@Component({
  selector: 'app-list-cards',
  standalone: true,
  imports: [CardComponent],
  templateUrl: './list-cards.component.html',
  styleUrl: './list-cards.component.css'
})
export class ListCardsComponent {
 cards !: Array<Card>;


 constructor( private cardService: CardService){}

 getCards(){
   this.cardService.getAllCards().subscribe(
     (data) => {
       this.cards=data;
       console.log(data);
       
     })
     
   }

 ngOnInit(){
   this.getCards();
 }

}
