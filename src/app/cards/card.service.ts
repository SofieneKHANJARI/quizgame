import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Card } from './card';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  baseAPI= "/assets/cards.json";
  
  constructor(private http: HttpClient) { }

  getAllCards():Observable<Array<Card>>{
    return this.http.get<Array<Card>>(this.baseAPI);
  }

  getCategory(id: number):Observable<Card>{
    return this.http.get<Card>(this.baseAPI);
  }

}
